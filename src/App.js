import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import AppRouter from './router';

import NavBar from './components/NavBar';

function App() {
  return (
    <>
      <NavBar />
      <Router>
        <AppRouter />
      </Router>
    </>
  );
}

export default App;
