import React, { useState, useEffect } from 'react';
import { Form, Col, Button, Modal, Alert} from "react-bootstrap";
import api from '../../../services/api';
import "react-datepicker/dist/react-datepicker.css";

function BankAccountForm(props) {
  const [show, setShow] = useState(false);
  const [errs, setErrs] = useState([]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [ name, setName ] = useState('');
  const [ agency, setAgency ] = useState('');
  const [ account, setAccount] = useState('');

  const saveBankAccount = async () => {
    try {
      let vf = validateFields();
      
      if(vf.length > 1) {
        setErrs(vf);
        setTimeout(() => setErrs([]), 6000);
        return false;
      }

      let data = { name, agency, account };

      await api.post('bank_accounts/', data);
      props.reload();
      resetFields();
      setShow(false);
    } catch(e) {
      console.log(e);
    }
  }

  const validateFields = () => {
    let errors = [];
    if(!account) errors.push('Digite uma CONTA para continuar');
    if(!agency) errors.push('Digite a AGENCIA de crédito para continuar');
    if(!name) errors.push('Digite um NOME para continuar');
    return errors;
  }

  const resetFields = () => {
    setName('');
    setAgency('');
    setAccount('');
  }

  useEffect(() => {
    
  }, []);

  return(
    <>
      <Button variant="primary" onClick={() => { 
          handleShow();
        }
      }>
        Nova Conta
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title>
          Nova Conta
        </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Col className={'mt-2'}>
            { 
              errs && errs.map((err, idx) => (
                <Alert key={idx} variant="danger"><p>{err}</p></Alert>))
            }
            <Form>
              <Form.Group>
                <Form.Control 
                  type="text" 
                  placeholder="Nome" 
                  onChange={(e) => setName(e.target.value)} />
              </Form.Group>
              <Form.Group>
                <Form.Control 
                  type="text" 
                  placeholder="Agency" 
                  onChange={(e) => setAgency(e.target.value)} />
              </Form.Group>
              <Form.Group>
                <Form.Control 
                  type="text" 
                  placeholder="Conta" 
                  onChange={(e) => setAccount(e.target.value)} />
              </Form.Group>
            </Form>
          </Col>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={saveBankAccount}>
            Inserir Conta
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default BankAccountForm;