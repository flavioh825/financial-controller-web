import React, { useState, useEffect } from 'react';
import api from '../../services/api';
import { Row, Col, Container, Table } from 'react-bootstrap';
import BankAccountForm from './Form';

function BankAccount() {

  const [ accounts, setAccounts ] = useState([]);

  useEffect(() => {
    getBankAccounts();
  }, [])

  async function getBankAccounts() {
    try {
      const response = await api.get('/bank_accounts');
      setAccounts(response.data);
    }catch(e){
      console.log(e);
    }
  }

  return (
    <>
      <Container fluid={true} className={'mt-2'}>
        <Row>
          <Col>
            <BankAccountForm 
              reload={() => getBankAccounts()} />
          </Col>
        </Row>
        <Table bordered hover size="sm" className={'mt-2'}>
          <thead>
            <tr>
              <th>Nome</th>
              <th>Agência</th>
              <th>Conta</th>
              <td></td>
            </tr>
          </thead>
          <tbody>
            { accounts.map((account, idx) => {
              return(
                <tr key={idx}>
                  <td>{account.name}</td>
                  <td>{account.account}</td>
                  <td>{account.agency}</td>
                  <td></td>
                </tr>
              )
            }) }
          </tbody>
        </Table>
      </Container>
    </>
  );
}

export default BankAccount;