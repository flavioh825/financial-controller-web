import React, { useState, useEffect } from 'react';
import { Form, Col, Button, Modal, Alert} from "react-bootstrap";
import api from '../../../services/api';
import "react-datepicker/dist/react-datepicker.css";

function CreditCardForm(props) {
  const [show, setShow] = useState(false);
  const [errs, setErrs] = useState([]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [ name, setName ] = useState('');
  const [ lastFourNumbers, setLastFourNumbers ] = useState('');
  const [ flagName, setFlagName] = useState('');
  const [ limit, setLimit] = useState(0.00);

  const saveCreditCard = async () => {
    try {
      let vf = validateFields();
      
      if(vf.length > 1) {
        setErrs(vf);
        setTimeout(() => setErrs([]), 6000);
        return false;
      }

      let data = { 
        name, 
        last_four_numbers: lastFourNumbers, 
        flag_name: flagName,
        limit
      };

      await api.post('credit_cards/', data);
      props.reload();
      resetFields();
      setShow(false);
    } catch(e) {
      console.log(e);
    }
  }

  const validateFields = () => {
    let errors = [];
    if(!flagName) errors.push('Selecione uma BANDEIRA para continuar');
    if(!name) errors.push('Digite um NOME para continuar');
    return errors;
  }

  const resetFields = () => {
    setName('');
    setLastFourNumbers('');
    setFlagName('');
    setLimit(0.00);
  }

  return(
    <>
      <Button variant="primary" onClick={() => { 
          handleShow();
        }
      }>
        Novo Cartão
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title>
          Novo Cartão
        </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Col className={'mt-2'}>
            { 
              errs && errs.map((err, idx) => (
                <Alert key={idx} variant="danger"><p>{err}</p></Alert>))
            }
            <Form>
              <Form.Group>
                <Form.Control 
                  type="text" 
                  placeholder="Nome" 
                  onChange={(e) => setName(e.target.value)} />
              </Form.Group>
              <Form.Group>
                <Form.Control 
                  type="text" 
                  placeholder="Últimos 4 dígitos" 
                  onChange={(e) => setLastFourNumbers(e.target.value)} />
              </Form.Group>
              <Form.Group>
                <Form.Control
                  as="select" 
                  value={flagName} 
                  onChange={(e) => setFlagName(e.target.value)}>
                  <option value="">Selecione</option>
                  <option value="MASTERCARD">MASTERCARD</option>
                  <option value="VISA">VISA</option>
                  <option value="ELO">ELO</option>
                  <option value="AMERICAN EXPRESS">AMERICAN EXPRESS</option>
                  <option value="HIPERCARD">HIPERCARD</option>
                  <option value="HIPERCARD">HIPER</option>
                  <option value="DINERS CLUB">DINERS CLUB</option>
                  <option value="REDE SHOP">REDE SHOP</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Control 
                  type="text" 
                  placeholder="Limite" 
                  onChange={(e) => setLimit(e.target.value)} />
              </Form.Group>
            </Form>
          </Col>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={saveCreditCard}>
            Inserir Cartão
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default CreditCardForm;