import React, { useState, useEffect } from 'react';
import api from '../../services/api';
import { Row, Col, Container, Table } from 'react-bootstrap';
import CreditCardForm from './Form';

function CreditCard() {

  const [ creditCards, setCreditCards ] = useState([]);

  useEffect(() => {
    getCreditCards();
  }, [])

  async function getCreditCards() {
    try {
      const response = await api.get('/credit_cards');
      setCreditCards(response.data);
    }catch(e){
      console.log(e);
    }
  }

  return (
    <>
      <Container fluid={true} className={'mt-2'}>
        <Row>
          <Col>
            <CreditCardForm 
              reload={() => getCreditCards()} />
          </Col>
        </Row>
        <Table bordered hover size="sm" className={'mt-2'}>
          <thead>
            <tr>
              <th>Nome</th>
              <th>4 últimos dígitos</th>
              <th>Bandeira</th>
              <td>Limite</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            { creditCards.map((cc, idx) => {
              return(
                <tr key={idx}>
                  <td>{cc.name}</td>
                  <td>{cc.last_four_numbers}</td>
                  <td>{cc.flag_name}</td>
                  <td>{cc.limit}</td>
                  <td></td>
                </tr>
              )
            }) }
          </tbody>
        </Table>
      </Container>
    </>
  );
}

export default CreditCard;