import React, { useState, useEffect } from 'react';
import { Form, Spinner, Col, Button, Modal, Alert} from "react-bootstrap";
import api from '../../../../services/api';
import { ptBR } from 'date-fns/locale';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

function CreditForm(props) {
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(true);
  const [errs, setErrs] = useState([]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [ bankAccounts, setBankAccounts ] = useState([]);
  const [ bankAccount, setBankAccount ] = useState('');
  const [ type, setType ] = useState('SALÁRIO');
  const [ value, setValue ] = useState(0.00);
  const [createdAt, setCreatedAt] = useState(new Date());

  const userBankAccounts = async () => {
    try {
      const response = await api.get('users/user_data');
      setBankAccounts(response.data.bankAccounts);
      setLoading(false);
    } catch(err) {
      return err;
    }
  }

  const saveCredit = async () => {
    try {
      let vf = validateFields();
      
      if(vf.length > 1) {
        setErrs(vf);
        setTimeout(() => setErrs([]), 6000);
        return false;
      }

      let data = {
        bank_account_id: bankAccount,
        type,
        value,
        created_at: createdAt
      };

      await api.post('credits/', data);
      props.reload();
      resetFields();
      setShow(false);
    } catch(e) {
      console.log(e);
    }
  }

  const validateFields = () => {
    let errors = [];
    if(!bankAccount) errors.push('Selecione uma CONTA para continuar');
    if(!type) errors.push('Selecione um TIPO de crédito para continuar');
    if(!value) errors.push('Digite um VALOR para continuar');
    if(!value <= 0.00) errors.push('O VALOR deve ser maior que 0.00');
    return errors;
  }

  const resetFields = () => {
    setBankAccounts([]);
    setBankAccount('');
    setType('SALARIO');
    setValue(0.00);
  }

  useEffect(() => {
    userBankAccounts();
  }, []);

  return(
    <>
      <Button variant="primary" onClick={() => { 
          handleShow();
          userBankAccounts();
        }
      }>
        Inserir Credito
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title>
          Inserir Crédito
        </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Col className={'mt-2'}>
            { 
              errs && errs.map((err, idx) => (
                <Alert key={idx} variant="danger"><p>{err}</p></Alert>))
            }
            {
              (!loading) ?
              (<Form>
                <Form.Group className={'text-center'}>
                  <DatePicker
                    locale={ptBR}
                    className={'form-control col-12'}
                    showPopperArrow={false}
                    dateFormat="dd/MM/yyyy HH:MM"
                    selected={createdAt}
                    onChange={ date => setCreatedAt(date) } />
                </Form.Group>
                <Form.Group>
                  <Form.Control as="select" onChange={(e) => setBankAccount(e.target.value)}>
                    <option>Selecione sua conta</option>
                    {
                      bankAccounts.map((ba, idx) => 
                        <option key={idx} value={ba.id}>{ba.name} - {ba.agency}</option>)
                    }
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Control as="select" onChange={(e) => setType(e.target.value)}>
                      <option value="SALÁRIO">SALÁRIO</option>
                      <option value="TRABALHO EXTRA">TRABALHO EXTRA</option>
                      <option value="GRATIFICAÇÃO">GRATIFICAÇÃO</option>
                      <option value="REEMBOLSO">REEMBOLSO</option>
                      <option value="DEVOLUÇÃO">DEVOLUÇÃO</option>
                      <option value="OUTRO">OUTRO</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Control type="text" placeholder="Valor" onChange={(e) => setValue(e.target.value)}/>
                </Form.Group>
              </Form>) :
              (<Spinner animation="border" variant="primary" />)
            }
          </Col>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={saveCredit}>
            Inserir Crédito
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default CreditForm;