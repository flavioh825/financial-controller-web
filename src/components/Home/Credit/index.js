import React, { useState, useEffect } from 'react';
import api from '../../../services/api';
import { Table } from 'react-bootstrap';

const Credit = (props) => { 

  const [ credits, setCredits ] = useState([]);

  const loadCredits = async () => {
    try {
      let params = { 
        params: { 
          user_id: props.user, 
          all: null,
          initialDate: props.startDate,
          finalDate: props.finalDate
        }
      }
      let response = await api.get('credits/', params);
      setCredits(response.data);
      console.log(response.data);
    } catch(err) {
      console.error(err);
    }
  }

  const sumCreditsValues = () => {
    if(credits.length === 0) return
    let sum = credits?.reduce((currVal, elem) => currVal + elem.value, 0);
    props.sumCredits(sum);
  }

  useEffect(() => {
    loadCredits();
  }, [
    props.reloadCredits,
    props.user, 
    props.startDate, 
    props.finalDate
  ]);

  useEffect(() => {
    sumCreditsValues();
  }, [credits]);

  return (
    <>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>Tipo</th>
            <th>Responsavel</th>
            <th>Valor</th>
            <th>Conta</th>
            <th>Data</th>
          </tr>
        </thead>
        <tbody>
          { credits.map((cr, idx) => {
              return (
                <tr key={idx}>
                  <td>{cr.type}</td>
                  <td>{cr.user.username}</td>
                  <td>{cr.value}</td>
                  <td>{cr.bankAccount?.name}</td>
                  <td>{cr.created_at}</td>
                </tr>
              )
            }) 
          }
        </tbody>
      </Table>
    </>
  );
};

export default Credit;