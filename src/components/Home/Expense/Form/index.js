import React, { useState, useEffect } from 'react';
import { Form, Col, Button, Modal, Alert, Card, Table} from "react-bootstrap";
import api from '../../../../services/api';
import { ptBR } from 'date-fns/locale';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { format } from 'date-fns';
import { currentDateTime } from '../../../../services/helper';

function ExpenseForm(props) {
  const [show, setShow] = useState(false);
  const [errs, setErrs] = useState([]);

  const [id, setId] = useState(0);
  const [description, setDescription] = useState('');
  const [formPayment, setFormPayment] = useState('dinherio');
  const [value, setValue] = useState(0.00);
  const [wasPaid, setWasPaid] = useState(format(new Date(currentDateTime()), 'yyyy-MM-dd HH:mm'));
  const [fixedExpense, setFixedExpense] = useState(false);
  const [createdAt, setCreatedAt] = useState(new Date());
  const [expense, setExpense] = useState({});

  const [ creditCards, setCreditCards ] = useState([]);
  const [ creditCardId, setCreditCardId ] = useState('');
  const [ installmentsAmount, setInstallmentAmount ] = useState('');
  const [ installmentsValue, setInstallmentsValue ] = useState('');

  const handleClose = () => { 
    setShow(false);
    resetFields();
    if(props.showModal) props.closeModal();
  }

  const handleShow = () => {
    setShow(true);
    console.log(props.activityInfo);
    if(Object.entries(props.activityInfo).length === 0) return
    loadExpense(props.activityInfo.expenseId);
    // if(props.activityInfo.activity === 'addNewValue')
  }

  const loadExpense = async (id) => {
    try {
      const response = await api.get(`/expenses/${id}`);
      setExpense(response.data);
      setId(response.data.id);
      setDescription(response.data.description);
      setFormPayment(response.data.form_payment);
      setValue(response.data.expenseValue.value);
      setWasPaid(response.data.expenseValue.was_paid);
      setFixedExpense(response.data.fixed_expense);
      setCreatedAt(new Date(response.data.created_at));
    } catch (e) {
      console.log(e);
    }
  }

  const saveExpense = async () => {
    try {
      let vf = validateFields();
      
      if(vf.length > 1) {
        setErrs(vf);
        setTimeout(() => setErrs([]), 6000);
        return false;
      }

      let data = {
        description,
        form_payment: formPayment,
        installments_amount: '',
        installments_value: '',
        fixed_expense: fixedExpense,
        created_at: createdAt,
        expense_value: {
          value,
          was_paid: wasPaid,
          created_at: createdAt
        }
      }
      const response = await api.post('expenses/', data);
      props.reload(response.data);
      resetFields();
      handleClose();
    } catch(e) {
      console.log(e);
    }
  }

  const validateFields = () => {
    let errors = [];
    if(!description) errors.push('O campo DESCRIÇÃO é obrigatório.');
    if(!formPayment) errors.push('Selecione uma FORMA DE PAGAMENTO para continuar');
    if(!createdAt) errors.push('Selecione uma DATA para continuar');
    if(!value) errors.push('Digite um VALOR para continuar');
    if(!value <= 0.00) errors.push('O VALOR deve ser maior que 0.00');
    return errors;
  }

  const resetFields = () => {
    setDescription('');
    setFormPayment('dinheiro');
    setValue(0.00);
    setWasPaid(true);
    setFixedExpense(false);
  }

  const newExpenseValue = async () => {
    try {
      let data = { value, was_paid: wasPaid, expense_id: id };
      console.log(data);
      const response = await api.post('/expense_values', data);
      props.reload(response.data);
      resetFields();
      handleClose();
    } catch (e) {
      console.log(e);
    }
  }

  const renderSaveButton = () => {
    if(props.activityInfo?.activity === 'addNewValue') {
      return (
        <Button variant="primary" onClick={newExpenseValue}>
          Novo valor
        </Button>
      );
    } else {
      return(
        <Button variant="primary" onClick={saveExpense}>
          Inserir Despesa
        </Button>
      );
    }
  }

  const loadUserCreditCards = async () => {
    try {
      const response = await api.get('/credit_cards');
      setCreditCards(response.data);
    } catch(e) {
      console.log(e);
    }
  }

  const isCredit = () => {
    if(formPayment === 'crédito') {
      return (
        <>
          <Form.Group>
            <Form.Control 
              disabled={props.activityInfo?.activity === 'addNewValue' ? true : false} 
              as="select"
              onChange={(e) => setCreditCardId(e.target.value)}>
                <option value="">Selecione um cartão</option>
                { creditCards.map((cd, idx) => {
                    return <option key={idx} value={cd.id}>{cd.name} - {cd.flag_name}</option>
                  })
                }
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              type="text" 
              placeholder="Quantidade de parcelas" 
              onChange={(e) => setInstallmentAmount(e.target.value)}>
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              type="text" 
              placeholder="Valor das parcelas" 
              onChange={(e) => setInstallmentsValue(e.target.value)}>
            </Form.Control>
          </Form.Group>
        </>
      )
    } else {
      return null;
    }
  }

  useEffect(() => {
    if(props.showModal){
      handleShow();
      setId(props.expenseId);
    }
  }, [props.showModal]);

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Inserir Despesa
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            Inserir Débito 
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Col className={'mt-2'}>
            { 
              errs && errs.map((err, idx) => (
                <Alert key={idx} variant="danger"><p>{err}</p></Alert>))
            }
            <Form>
              <Form.Group className={'text-center'}>
                <DatePicker
                  disabled={props.activityInfo?.activity === 'addNewValue' ? true : false}
                  locale={ptBR}
                  className={'form-control col-12'}
                  showPopperArrow={false}
                  dateFormat="dd/MM/yyyy"
                  selected={createdAt}
                  onChange={ date => setCreatedAt(date) } />
              </Form.Group>
              <Form.Group>
                <Form.Control
                  disabled={props.activityInfo?.activity === 'addNewValue' ? true : false}
                  type="text" 
                  placeholder="Descrição"
                  onChange={(e) => setDescription(e.target.value)}
                  value={description}
                />
              </Form.Group>
              <Form.Group>
                <Form.Control 
                  disabled={props.activityInfo?.activity === 'addNewValue' ? true : false} 
                  as="select" 
                  value={formPayment} 
                  onChange={(e) => {
                      setFormPayment(e.target.value);
                      isCredit();
                      loadUserCreditCards();
                    }
                  }>
                  <option value="dinheiro">Dinheiro</option>
                  <option value="crédito">Crédito</option>
                  <option value="débito">Débito</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Control 
                  type="text" 
                  placeholder="Valor" 
                  onChange={(e) => setValue(e.target.value)} />
              </Form.Group>
              { isCredit() }
              <Form.Group>
                <Form.Check 
                  disabled={props?.activityInfo?.activity === 'addNewValue' ? true : false}
                  type='checkbox'
                  id="fixed_account"
                  label={`É uma conta fixa?`}
                  checked={fixedExpense ? true : false}
                  onChange={(e) => !fixedExpense ? setFixedExpense(true) : setFixedExpense(false)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type='checkbox'
                  id="was_paid"
                  label={`Está pago?`}
                  checked={wasPaid !== null ? true : false}
                  onChange={() => setWasPaid(format(new Date(currentDateTime()), 'yyyy-MM-dd HH:mm'))}
                />
              </Form.Group>
            </Form>
          </Col>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          { renderSaveButton() }
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ExpenseForm;