import React, { useState, useEffect } from 'react';
import api from '../../../services/api';
import { Table } from 'react-bootstrap';
import { FaPlusCircle, FaDollarSign } from 'react-icons/fa';
import { format } from 'date-fns';

const Expense = (props) => {

  const [ expenses, setExpenses ] = useState([]);

  const loadExpenses = async () => {
    try {
      let params = { 
        params: { 
          user_id: props.user, 
          all: null,
          initialDate: format(props.startDate, 'yyyy-MM-dd'),
          finalDate: format(props.finalDate, 'yyyy-MM-dd')
        }
      }
      let response = await api.get('expenses/', params);
      setExpenses(response.data);
      // props.onChange();
    } catch(err) {
      console.error(err);
    }
  }

  const sumExpensesValues = () => {
    if(expenses.length === 0) return
    let sum = expenses?.reduce((currVal, elem) => {
      return currVal + parseFloat(elem.expenseValue?.value);
    }, 0)
      
    props.sumExpenses(sum.toFixed(2));
  }

  useEffect(() => {
    loadExpenses();
  }, [
    props.reloadExpenses,
    props.user, 
    props.startDate, 
    props.finalDate
  ]);

  useEffect(() => {
    sumExpensesValues();
  }, [expenses]);

  return(
    <>
      <Table bordered hover size="sm">
        <thead>
          <tr>
            <th>Descrição</th>
            <th>Responsavel</th>
            <th>Forma de pagamento</th>
            <th>Valor</th>
            <th>Situação</th>
            <th>Data</th>
            <td></td>
          </tr>
        </thead>
        <tbody>
          { expenses.map((ex, idx) => {
            return (
              <tr key={idx} 
                  className={ex.fixed_expense ? 'alert-warning' : ''} 
                  style={{border: '3px solid #cccccc'}}>
                <td>{ex.description}</td>
                <td>{ex.user.username}</td>
                <td>{ex.form_payment}</td>
                <td>{ex.expenseValue?.value}</td>
                <td 
                  className={ex.expenseValue?.was_paid ? 'alert-success' : 'alert-danger'}>
                  {ex.expenseValue?.was_paid}
                </td>
                <td>{ex.created_at}</td>
                <td>
                  {
                    (ex.fixed_expense) ? 
                    (
                      <button 
                        type="button" 
                        className={"btn btn-primary btn-sm"} 
                        title="Adicionar novo valor"
                        onClick={() => props.loadFormExpense({ 
                          id: ex.id, 
                          modal: 'formExpense',
                          activity: 'addNewValue'
                        })}>
                        <FaPlusCircle />
                        <FaDollarSign />
                      </button>
                    ) : (null)}
                </td>
              </tr>
            )
            })
          }
        </tbody>
      </Table>
    </>
  );
};

export default Expense;