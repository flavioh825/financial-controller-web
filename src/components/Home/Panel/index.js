import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';

function Panel(props) {

  return(
    <>
      <Col className={'mt-2'}>
        <Card>
          <Card.Body>
            <Row>
              <Col sm={8}>
                <strong>Total de despesas: {props.totalExpenses}</strong>
              </Col>
              <Col sm={4} className={'text-right'}>
                <strong>Total na conta: {(props.totalCredits - props.totalExpenses).toFixed(2)}</strong>
              </Col>
            </Row>
          </Card.Body>
        </Card>
      </Col>
    </>
  )
}

export default Panel;