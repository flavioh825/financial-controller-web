import React, { useState, useEffect, useRef } from 'react';
import api from '../../services/api';
import { Container, Row, Col, Form, FormControl } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import { ptBR } from 'date-fns/locale';
import './style.css';
import "react-datepicker/dist/react-datepicker.css";

import ExpenseForm from './Expense/Form';
import CreditForm from './Credit/Form';
import Expense from './Expense';
import Credit from './Credit';
import Panel from './Panel';

function Home() {
  
  let firstDayOfMonth = () => {
    return new Date(new Date().getFullYear(), new Date().getMonth(), 1);
  }

  const [ users, setUsers ] = useState([]);
  const [ user, setUser ] = useState('');
  const [ type, setType ] = useState(0);
  const [ startDate, setStartDate ] = useState(firstDayOfMonth());
  const [ finalDate, setFinalDate ] = useState(new Date());
  const [ totalExpenses, setTotalExpenses ] = useState(0.00);
  const [ totalExpensesValue, setTotalExpensesValue ] = useState(0.00);
  const [ totalCreditsValue, setTotalCreditsValue ] = useState(0.00);

  const [ savedExpense, setSavedExpense ] = useState({});
  const [ savedCredit, setSavedCredit ] = useState({});

  const [ showFormExpense, setShowFormExpense ] = useState(false);
  const [ expenseActivityInfo, setExpenseActivityInfo ] = useState(0);
  const [ showFormCredit, setShowFormCredit ] = useState(false);
  const [ creditId, setCreditId ] = useState(0);

  async function loadUsers() {
    try {
      const response = await api.get('/users');
      setUsers(response.data);
    }catch(err) {
      console.error(err);
    }
  }

  let loadModal = (data) => {
    if(data.modal === 'formExpense') {
      setShowFormExpense(true);
      setExpenseActivityInfo({ expenseId: data.id, activity: data.activity });
    } else if(data.modal === 'formCredit') {
      setShowFormCredit(true);
      setCreditId(data.id);
    }
  }

  useEffect(() => {
    loadUsers();
  }, [user, startDate, finalDate, type]);

  return (
    <>
      <Container fluid={true} className={'mt-2'}>
        <Row>
          <Col className={'text-left'}>
            <ExpenseForm
              activityInfo={expenseActivityInfo}
              showModal={showFormExpense}
              closeModal={() => { 
                setShowFormExpense(false);
                setExpenseActivityInfo({});
              }}
              reload={value => setSavedExpense(value)} />
          </Col>
          <Col className={'text-right'}>
            <CreditForm
              creditId={creditId}
              showModal={showFormCredit}
              closeModal={() => setShowFormCredit(false)}
              reload={value => setSavedCredit(value)} />
          </Col>
        </Row>
        <Row>
          <Col className={'mt-2'}>
            <Form>
              <Row>
                <Col sm="2" className={'mt-2'}>
                  <FormControl 
                    as="select"
                    onChange={(e) => setType(e.target.value)}>
                    <option value="0">Despesas</option>
                    <option value="1">Créditos</option>
                  </FormControl>
                </Col>
                <Col sm="2" className={'mt-2'}>
                  <FormControl 
                    as="select" 
                    onChange={ (e) => setUser(e.target.value) }>
                    <option value={''}>Todos</option>
                    {
                      users.map((user, idx) => {
                        return <option key={idx} value={user.id}>{user.email}</option>;
                      })
                    }
                  </FormControl>
                </Col>
                <Col sm="2" className={'mt-2'}>
                  <DatePicker
                    locale={ptBR}
                    className={'form-control'}
                    showPopperArrow={false}
                    dateFormat="dd/MM/yyyy"
                    selected={startDate}
                    onChange={ date => setStartDate(date) }
                  />
                </Col>
                <Col sm="2" className={'mt-2'}>
                  <DatePicker
                    locale={ptBR}
                    className={'form-control'}
                    showPopperArrow={false}
                    dateFormat="dd/MM/yyyy"
                    selected={finalDate}
                    onChange={ date => setFinalDate(date) }
                  />
                </Col>
              </Row>
              <Row>
                <Panel 
                  expenses={totalExpenses} 
                  totalExpenses={totalExpensesValue}
                  totalCredits={totalCreditsValue} />
              </Row>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col className={`mt-2 ${type == 0 ? 'show': 'hide'}`}>
            <Expense
              loadFormExpense={loadModal}
              reloadExpenses={savedExpense}
              sumExpenses={(value) => setTotalExpensesValue(value)}
              user={user} 
              startDate={startDate} 
              finalDate={finalDate} />
          </Col>
          <Col className={`mt-2 ${type == 1 ? 'show': 'hide'}`}>
            <Credit
              reloadCredits={savedCredit}
              sumCredits={(value) => setTotalCreditsValue(value)}
              user={user}
              startDate={startDate} 
              finalDate={finalDate} />
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Home;