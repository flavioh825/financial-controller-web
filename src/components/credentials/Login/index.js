import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import api from '../../../services/api';
import { login, isAuthenticated } from '../../../services/auth';
import './style.css';
import { 
  Container, 
  Row, 
  Col, 
  Card, 
  Form, 
  FormGroup,
  Button, Alert } from 'react-bootstrap';

function Login(props) {

  function verifyAuth() {
    if(isAuthenticated()) {
      return props.history.push('/home');
    }
    return;
  }

  let [ email, setEmail ] = useState('');
  let [ password, setPassword ] = useState('');
  let [ errorMessage, setErrorMessage ] = useState('');

  async function signIn() {
    if(!email || !password) {
      setErrorMessage('Preencha e-mail e senha para prosseguir.');
    } else {
      try {
        const response = await api.post('/sessions', { email, password });
        login(response.data.token);
        props.history.push('/home');
      } catch (error) {
        setErrorMessage('Houve um erro ao tentar fazer o login.');
      }
    }
  }

  useEffect(() => {
    verifyAuth();
  });

  return (
    <Container>
      <Row>
        <Col md={{ span: 6, offset: 3 }}>
          <Card className="login-card">
            <Card.Body>
              <Card.Title>Login</Card.Title>
              { errorMessage !== '' ? (
                  <Alert variant="danger">{ errorMessage }</Alert>) : null }
              <Form>
                <FormGroup>
                  <Form.Control
                    onKeyUp={ (event) => setEmail(event.target.value) }
                    type="text" 
                    placeholder="Email" />
                </FormGroup>
                <FormGroup>
                  <Form.Control 
                    onKeyUp={ (event) => setPassword(event.target.value) }
                    type="password" 
                    placeholder="Senha" />
                </FormGroup>
                <Button 
                  onClick={ signIn }
                  className="col-12" 
                  type="button">Entrar</Button>
              </Form>
              <Link to="/home">GoTo Home</Link>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Login;