import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { isAuthenticated } from '../../../services/auth';

function SignUp(props) {

  function verifyAuth() {
    if(isAuthenticated()) {
      return props.history.push('/home');
    }
    return;
  }

  useEffect(() => {
    verifyAuth();
  });

  return(
    <>
      <div>Minha tela de cadastro</div>
      <Link to="/">GoTo Login</Link>
    </>
  );
}

export default SignUp;