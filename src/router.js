import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';

import { isAuthenticated } from './services/auth';

import Login from './components/credentials/Login';
import Home from './components/Home';
import SignUp from './components/credentials/SignUp';
import BankAccount from './components/BankAccount';
import CreditCard from './components/CreditCard';

const RequireAuth = ({ children }) => {
  if(!isAuthenticated()) {
    return <Redirect to={'/'} />;
  }
  return children;
}

const AppRouter = () => (
  <Switch>
    <Route exact path={'/'} component={Login} />
    <Route exact path={'/cadastrar'} component={SignUp} />
    <RequireAuth>
      <Route exact path={'/home'} component={Home} />
      <Route exact path={'/bank_accounts'} component={BankAccount} />
      <Route exact path={'/credit_cards'} component={CreditCard} />
    </RequireAuth>
  </Switch>
);

export default AppRouter;