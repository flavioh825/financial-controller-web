import axios from 'axios';
import { getToken, logout } from './auth';

const api = axios.create({
  baseURL: 'http://127.0.0.1:3333'
});

api.interceptors.request.use(async config => {
  const token = getToken();
  if(token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

api.interceptors.response.use(response => {
  return response;
}, error => {
  const res = error.response.data;
  if(res.error.status === 401 && res.error.name === "InvalidJwtToken") {
    return logout();
  } else {
    return Promise.reject(error);
  }
});

export default api;