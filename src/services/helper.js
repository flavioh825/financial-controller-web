import React from 'react';

export const lastInserted = (array) => {
  let maxId = array?.reduce((prev, el) => Math.max(prev, el.id), 0);
  return array?.filter((elem) => elem.id === maxId)[0];
}

export const releaseObject = (array) => {
  return array[0];
}

export const currentDateTime = () => {
  let date = new Date();
  let current = new Date(date.valueOf() - date.getTimezoneOffset() * 60000);
  return current.toISOString().replace(/\.\d{3}Z$/, '');
}